x = 3 # naredba, postavljanje vrijednosti
# x je (varijabla) zamjenica, ime
# zamjenica za objekt
# ime objekta

print("Hello, world!")

# pojedinačni
# brojevi (cijeli, int)

# brojevi s pomičnim zarezom (float)

# zbirni tipovi:
#
# string (niz znakova), navodnici
# lista (niz objekata), uglate zgrade
# skup - vitičaste
# n-tokra (entorka; tuple), obične zagrade

# ime objekta <TOČKA> izraz
# -> slanje poruke objektu


for x in range(-2,10):
    print(x, end=" ")
    if 5 > x > 3:
        print("prva opcija")
    elif x >= 5 and x < 7:
        print("druga opcija")
    elif x <= 3 or x > 7:
        print("treća opcija")
    else:
        print("četvrta opcija")


import math

for x in {"A", "kvaka", 4, math.pi}:
    print(x)


import string

for slovo in "ABCČĆDĐEFGHIJKLMNOPRSŠTUVZŽ":
    print(slovo)

x = 39456348670928760298734069827306084568

while x > 7:
    print(x)
    x //= 12 # x-u pridruži kvocijent x//12

lista_stringova = ["349643896", "bla", "bla32"]

for naš_string in lista_stringova:
    if all(letter in string.digits
           for letter in naš_string):
        print(naš_string, "sve su znamenke")
        print(int(naš_string)+3)
    else:
        print(naš_string, "nešto nije znamenka")

def f(x,y): # definicija funkcije
    return x+y

def g(x,y): # definicija funkcije
    print(x+y)
