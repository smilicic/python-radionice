import pygame # uvozi modul pygame u naš kontekst
from pygame import Color # uvozimo klasu Color iz modula pygame u naš kontekst
from itertools import cycle

pygame.init()

# prozor - objekt koji definira prozor
#          širine 700px, visine 700px

prozor = pygame.display.set_mode((700,700))

# boje koje koristimo
# RGB (red green blue), 0-255 vrijednosti
siva = Color(200,200,200)
tamnosiva = Color(100,100,100)
bijela = Color(255,255,255)
crna = Color(0,0,0)
crvena = Color(255,0,0)
žuta = Color(220,220,0)
plava = Color(0,0,255) # RGB, vrijednosti 0..255

# tajmer koji koristimo za usklađiavnje s prikazom

clock = pygame.time.Clock()

gorivo = 5000 # početno gorivo
GRAVITACIJA = 500
POGONSKA_SILA = -300
T = 0 # ukupno vrijeme
delta_t = 0.01 # komadić vremena s kojim računamo
v_položaj, v_brzina = 100, 0 # početni vertikalni položaj i brzina
h_položaj, h_brzina = 350, 0 # početni vertikalni položaj i brzina
boja = plava # polazna boja landera
glavni_pogon_uključen = False
desni_pogon_uključen = False # ubrzava nas lijevo
lijevi_pogon_uključen = False # ubrzava nas desno
rep = cycle([4, -4, 0, 5, -3])
ime_fonta = pygame.font.get_default_font()
font = pygame.font.Font(ime_fonta, 24)

slika_landera = pygame.image.load('lander.png').convert_alpha()



pygame.draw.rect(prozor,
                 siva,
                 (0,0,700,700))

text_surface = font.render("Pritisnite razmaknicu.",
                           True, (0,0,0))

okvir = text_surface.get_rect(center=(350,350))
prozor.blit(text_surface, okvir)
pygame.display.update()

stisnuta_razmaknica = False
while not stisnuta_razmaknica:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                stisnuta_razmaknica = True

# glavna petlja igre
while v_položaj <= 600:
    # obrada pristiglih "događaja"
    # događaji mogu biti pomak miša, pomak prozora, interakcija s
    # tipkovnicom
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP: #strelica gore
                glavni_pogon_uključen = True
            if event.key == pygame.K_RIGHT: #strelica gore
                lijevi_pogon_uključen = True
            if event.key == pygame.K_LEFT: #strelica gore
                desni_pogon_uključen = True
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_UP: #strelica gore
                glavni_pogon_uključen = False
            if event.key == pygame.K_RIGHT: #strelica gore
                lijevi_pogon_uključen = False
            if event.key == pygame.K_LEFT: #strelica gore
                desni_pogon_uključen = False

    # rect = rectangle - pravokutnik
    pygame.draw.rect(prozor,
                     tamnosiva,
                     (0,0,700,650))

    pygame.draw.rect(prozor,
                     bijela,
                     (0,650,700,700))

    # pogon
    if not glavni_pogon_uključen or gorivo <= 0:
        v_akceleracija = GRAVITACIJA
        boja = plava

    h_akceleracija = 0
    if gorivo > 0:
        if glavni_pogon_uključen:
            gorivo -= 1
            v_akceleracija = POGONSKA_SILA
            boja = crvena
            pygame.draw.polygon(prozor,
                                žuta,
                                ((h_položaj-10, v_položaj + 55),
                                 (h_položaj+10, v_položaj + 55),
                                 (h_položaj, v_položaj + 80 + next(rep))))

        if lijevi_pogon_uključen: # gura nas desno
            gorivo -= 1
            boja = crvena
            h_akceleracija += 300
            pygame.draw.polygon(prozor,
                                žuta,
                                ((h_položaj-55, v_položaj - 10),
                                 (h_položaj-55, v_položaj + 10),
                                 (h_položaj-80+next(rep), v_položaj)))
        if desni_pogon_uključen: # gura nas lijevo
            gorivo -= 1
            boja = crvena
            h_akceleracija -= 300
            pygame.draw.polygon(prozor,
                                žuta,
                                ((h_položaj+55, v_položaj - 10),
                                 (h_položaj+55, v_položaj + 10),
                                 (h_položaj+80-next(rep), v_položaj)))

    okvir_landera = slika_landera.get_rect(center=(h_položaj, v_položaj))
    prozor.blit(slika_landera, okvir_landera)

    # pygame.draw.circle(prozor,
    #                    bijela,
    #                    (int(h_položaj), int(v_položaj)),
    #                    52)

    # pygame.draw.circle(prozor,
    #                    boja,
    #                    (int(h_položaj), int(v_položaj)),
    #                    50)

    v_položaj += v_brzina * delta_t
    v_brzina += v_akceleracija * delta_t

    h_položaj += h_brzina * delta_t
    h_brzina += h_akceleracija * delta_t

    display_brzine = font.render(f"{int(v_brzina)}", True, (255,255,255))
    display_goriva = font.render(f"{int(gorivo)}", True, (255,255,255))

    okvir = display_brzine.get_rect(topright=(690, 10))
    prozor.blit(display_brzine, okvir)
    okvir = display_goriva.get_rect(topright=(690, 40))
    prozor.blit(display_goriva, okvir)

    pygame.display.update()
    print(gorivo, v_brzina)
    clock.tick(60) # dočekaj sljedeću šezdesetinku senkunde

if v_brzina < 100:
    print("Touchdown")
else:
    print("CRASH!")
pygame.quit()
